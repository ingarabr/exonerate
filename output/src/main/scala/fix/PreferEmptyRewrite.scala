package fix

object PreferEmptyRewrite {
  val list1: List[Int] = List.empty
  val set1: Set[Int] = Set.empty
  val map1: Map[Int, Int] = Map.empty
  val seq1: Seq[Int] = Seq.empty

  val list2 = List.empty[Int]
  val set2 = Set.empty[Int]
  val map2 = Map.empty[Int, Int]
  val seq2 = Seq.empty[Int]
}
