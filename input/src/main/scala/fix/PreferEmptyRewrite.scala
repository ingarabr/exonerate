/*
rule = PreferEmpty
 */
package fix

object PreferEmptyRewrite {
  val list1: List[Int] = List()
  val set1: Set[Int] = Set()
  val map1: Map[Int, Int] = Map()
  val seq1: Seq[Int] = Seq()

  val list2 = List[Int]()
  val set2 = Set[Int]()
  val map2 = Map[Int, Int]()
  val seq2 = Seq[Int]()
}
