/*
rule = UnnecessaryToCall
exonerate.lint = false
 */
package fix

object UnnecessaryToCallRewrite {

  class A()

  val a = new A()

  val aStr1: String = a.toString
  val str1 = ""

  val aStr2: String = a.toString
  val str2 = ""

  List(1).toList.map(_ + 1)
  Set(1).toSet[Int].map(_ + 1)
  Map("1" -> 1).toMap.mapValues(_ + 1)

  List(1).map(_ + 1)
  Set(1).map(_ + 1)
  Map("1" -> 1).mapValues(_ + 1)
}
