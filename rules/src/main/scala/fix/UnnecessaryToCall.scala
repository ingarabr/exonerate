package fix

import scalafix.patch.Patch
import scalafix.v1._

import scala.meta.Term

class UnnecessaryToCall extends SemanticRule("UnnecessaryToCall") {

  val lookups =
    Map(
      "toString" -> "java.lang.String",
      "toList" -> "scala.collection.immutable.List",
      "toSet" -> "scala.collection.immutable.Set",
      "toMap" -> "scala.collection.immutable.Map",
    )

  override def fix(implicit doc: SemanticDocument): Patch =
    doc.tree.collect {

      case sel @ Term.Select(termToKeep, Term.Name(name))
          if lookups.get(name).contains(termType(sel)) =>
        Patch.replaceTree(
          sel.parent.collect { case ap: Term.ApplyType => ap }.getOrElse(sel),
          termToKeep.syntax
        )

    }.asPatch

  private def termType(term: Term)(implicit doc: SemanticDocument) =
    term.symbol.owner.normalized.toString().replaceAll("\\.$", "")
}
