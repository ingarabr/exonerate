package fix

import scalafix.v1._

import scala.meta._

class PreferEmpty extends SemanticRule("PreferEmpty") {

  private val names = List("List", "Map", "Seq", "Set")

  override def fix(implicit doc: SemanticDocument): Patch = {
    def matchName(name: String, parent: Term): Boolean = {
      val symName = parent.symbol.normalized.value
      names.contains(name) && (
        symName.startsWith("scala.collection") ||
        symName.startsWith("scala.Predef")
      )
    }

    def typeArgsStr(typeArgs: List[Type]) = {
      val types = typeArgs.collect { case Type.Name(n) => n }
      if (types.isEmpty) "" else types.mkString("[", ", ", "]")
    }

    doc.tree.collect {

      case t @ Term.Apply(Term.Name(name), List()) if matchName(name, t.fun) =>
        Patch.replaceTree(t, s"$name.empty")

      case t @ Term.ApplyType(Term.Name(name), typeArgs)
          if matchName(name, t.fun) =>
        t.parent
          .flatMap(_.collect { case t: Term.Apply => t }.headOption)
          .map(Patch.replaceTree(_, s"$name.empty${typeArgsStr(typeArgs)}"))
          .getOrElse(Patch.empty)

    }.asPatch
  }

}
